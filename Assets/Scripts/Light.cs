﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Light : MonoBehaviour {

    private CharController Сharcontroller;

	// Use this for initialization
	void Start ()
    {
        Сharcontroller = FindObjectOfType<CharController>();
    }
	
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            Сharcontroller.Health--;
            if (Сharcontroller.Health < 1)
            {
                Destroy(GameObject.Find("Player"));
            } 
        }
    }
}
