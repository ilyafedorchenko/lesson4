﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightGenerator : MonoBehaviour {

    private Quaternion rotation;
    public GameObject LightPrefab;

    
    // Instantiate the prefab somewhere between -10.0 and 10.0 on the x-z plane 
    void Start()
    {

        rotation = transform.rotation;
        InvokeRepeating("SpawnLight",0f,1f);

    }

    void SpawnLight()
    {
        Vector3 position = new Vector3(Random.Range(-6.0f, 6.0f), 5, Random.Range(-6.0f, 6.0f));
        GameObject _light = Instantiate(LightPrefab, position, rotation);
        Destroy(_light, 1f);
    }
}
